meta:
  id: crack_sensors_packet
  endian: be
  bit-endian: be
  title: LoRa Crack sensors packet
  doc-ref: https://cernbox.cern.ch/s/JGcyFAzgRrdltWV

seq:
 - id: magic
   contents: [0x00]
   doc: Frame magic byte
 - id: temperature_raw_hidden
   type: s2
   doc: Raw temperature reading from the sensor. The actual value can be calculated as temperature_raw * 0.01.
 - id: battery_voltage_raw_hidden
   type: u1
   doc: Raw battery voltage reading from the sensor. The actual value can be calculated as battery_voltage_raw * 4.71 + 2500.
 - id: displacement_raw
   type: f4
   doc: Displacement raw value  in mV/V.
 - id: displacement_physical
   type: f4
   doc: Displacement physical value in mm.

instances:
  temperature:
    value: temperature_raw_hidden * 0.01 + 0
    doc: Temperature reading in degrees Celsius.
  battery_voltage:
    value: battery_voltage_raw_hidden * 4.71 + 2500
    doc: Battery voltage in mV.
