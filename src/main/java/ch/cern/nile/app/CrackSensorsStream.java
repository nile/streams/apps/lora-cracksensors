package ch.cern.nile.app;

import ch.cern.nile.app.generated.CrackSensorsPacket;
import ch.cern.nile.kaitai.streams.LoraDecoderStream;

public final class CrackSensorsStream extends LoraDecoderStream<CrackSensorsPacket> {

    public CrackSensorsStream() {
        super(CrackSensorsPacket.class);
    }

}
