package ch.cern.nile.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.google.gson.JsonObject;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;

import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;

public class CrackSensorsStreamTest extends StreamTestBase {

    private static final JsonObject DATA_FRAME = TestUtils.loadRecordAsJson("data/data_frame.json");

    @Override
    public CrackSensorsStream createStreamInstance() {
        return new CrackSensorsStream();
    }

    @Test
    void givenDataFrame_whenDecoding_thenCorrectOutputRecordIsCreated() {
        pipeRecord(DATA_FRAME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        assertEquals(18.55, outputRecord.value().get("temperature").getAsDouble());
        assertEquals(3545.62, outputRecord.value().get("batteryVoltage").getAsDouble());
        assertEquals((float) -112.710335, outputRecord.value().get("displacementRaw").getAsFloat());
        assertEquals((float) 3.4965203, outputRecord.value().get("displacementPhysical").getAsFloat());
        assertNotNull(outputRecord.value().get("timestamp"), "Timestamp is null");
    }

}