<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ch.cern.nile</groupId>
    <artifactId>lora-cracksensors</artifactId>
    <version>1.1.0</version>

    <properties>
        <maven.compiler.release>21</maven.compiler.release>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <shadow.jar.suffix>jar-with-dependencies</shadow.jar.suffix>
    </properties>

    <dependencies>
        <dependency>
            <groupId>ch.cern.nile</groupId>
            <artifactId>nile-kaitai</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>ch.cern.nile</groupId>
            <artifactId>nile-common</artifactId>
            <version>1.0.0</version>
        </dependency>
        <dependency>
            <groupId>com.github.spotbugs</groupId>
            <artifactId>spotbugs-annotations</artifactId>
            <version>4.8.4</version>
        </dependency>
        <dependency>
            <groupId>ch.cern.nile</groupId>
            <artifactId>nile-test-utils</artifactId>
            <version>1.0.0</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <!-- Checkstyle plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>3.3.1</version>
                <configuration>
                    <configLocation>
                        https://gitlab.cern.ch/nile/java-build-tools/-/raw/master/code-quality/checkstyle.xml?ref_type=heads
                    </configLocation>
                    <suppressionsLocation>
                        https://gitlab.cern.ch/nile/java-build-tools/-/raw/master/code-quality/checkstyle-suppressions.xml?ref_type=heads
                    </suppressionsLocation>
                    <consoleOutput>true</consoleOutput>
                    <failsOnError>true</failsOnError>
                    <linkXRef>false</linkXRef>
                </configuration>
            </plugin>

            <!-- PMD plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <version>3.24.0</version>
                <dependencies>
                    <dependency>
                        <groupId>net.sourceforge.pmd</groupId>
                        <artifactId>pmd-core</artifactId>
                        <version>7.4.0</version>
                    </dependency>
                    <dependency>
                        <groupId>net.sourceforge.pmd</groupId>
                        <artifactId>pmd-java</artifactId>
                        <version>7.4.0</version>
                    </dependency>
                </dependencies>
                <configuration>
                    <linkXRef>false</linkXRef>
                    <rulesets>
                        <ruleset>
                            https://gitlab.cern.ch/nile/java-build-tools/-/raw/master/code-quality/pmd_java_ruleset.xml?ref_type=heads
                        </ruleset>
                    </rulesets>
                    <includeTests>true</includeTests>
                    <failOnViolation>true</failOnViolation>
                    <printFailingErrors>true</printFailingErrors>
                    <excludes>
                        <exclude>**/*Packet.java</exclude>
                        <exclude>**/*Test.java</exclude>
                    </excludes>
                </configuration>
            </plugin>

            <!-- SpotBugs plugin -->
            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
                <version>4.8.4.0</version>
                <configuration>
                    <effort>Max</effort>
                    <xmlOutput>false</xmlOutput>
                    <htmlOutput>true</htmlOutput>
                    <plugins>
                        <plugin>
                            <groupId>com.h3xstream.findsecbugs</groupId>
                            <artifactId>findsecbugs-plugin</artifactId>
                            <version>1.12.0</version>
                        </plugin>
                    </plugins>
                    <excludeFilterFile>
                        https://gitlab.cern.ch/nile/java-build-tools/-/raw/master/code-quality/spotbugs-exclude.xml?ref_type=heads
                    </excludeFilterFile>
                </configuration>
            </plugin>

            <!-- Compiler plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.0</version>
            </plugin>

            <!-- Surefire plugin for testing -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M5</version>
            </plugin>

            <!-- Assembly plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                        <configuration>
                            <archive>
                                <manifest>
                                    <mainClass>ch.cern.nile.common.StreamingApplication</mainClass>
                                </manifest>
                            </archive>
                            <descriptorRefs>
                                <descriptorRef>${shadow.jar.suffix}</descriptorRef>
                            </descriptorRefs>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>gitlab-maven-nile-kaitai</id>
            <url>https://gitlab.cern.ch/api/v4/projects/171127/packages/maven</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>gitlab-maven-nile-common</id>
            <url>https://gitlab.cern.ch/api/v4/projects/170995/packages/maven</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>gitlab-maven-nile-test-utils</id>
            <url>https://gitlab.cern.ch/api/v4/projects/171116/packages/maven</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>apache.snapshots</id>
            <name>Apache Snapshot Repository</name>
            <url>https://repository.apache.org/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>
</project>
