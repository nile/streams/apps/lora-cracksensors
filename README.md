# LoRa CrackSensors

## Overview

### Owners & Administrators

| Title       | Details                                                                                                                  |
|-------------|--------------------------------------------------------------------------------------------------------------------------|
| **E-group** | [lora-be-gm-asg-crack-sensors-admin](https://groups-portal.web.cern.ch/group/lora-be-gm-asg-crack-sensors-admin/details) |
| **People**  | [Kacper Widurch](https://phonebook.cern.ch/search?q=Kacper+Widurch)                                                      |

### Kafka Topics

| Environment    | Topic Name                                                                                                                  |
|----------------|-----------------------------------------------------------------------------------------------------------------------------|
| **Production** | [lora-cracksensors](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-cracksensors)                 |
| **Production** | [lora-cracksensors-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-cracksensors-decoded) |
| **QA**         | [lora-cracksensors](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-cracksensors)                 |
| **QA**         | [lora-cracksensors-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-cracksensors-decoded) |

### Configuration

| Title                        | Details                                                                                                    |
|------------------------------|------------------------------------------------------------------------------------------------------------|
| **Application Name**         | lora-BE-gm-asg-crack-sensors                                                                               |
| **Configuration Repository** | [app-configs/lora-cracksensors](https://gitlab.cern.ch/nile/streams/app-configs/lora-cracksensors) |

### Technologies

- LoRa